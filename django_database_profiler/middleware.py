from django.db import connection

class DatabaseProfilerMiddleware(object):
    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        response = self.get_response(request)
        return self.process_response(request, response)

    def process_request(self, request):
        pass

    def process_response(self, request, response):
        total_db_time = sum([float(query['time']) for query in connection.queries])
        total_queries = len(connection.queries)

        print('[DATABASE PROFILER] Base URL: %s' % request.build_absolute_uri());
        print('[DATABASE PROFILER] Total DB time: %.4fs. Total queries: %d' % (total_db_time, total_queries));

        return response
