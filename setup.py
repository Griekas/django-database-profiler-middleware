from setuptools import setup
from django_database_profiler import __version__

setup(
    name='Django Database Profiler Middleware',
    version=__version__,
    author='Sarunas Kujalis',
    author_email='sarunas@kujalis.lt',
    license='MIT',
    packages=['django_database_profiler'],
    zip_safe=False,
)
